package com.atlassian.ta.wiremockpactgenerator.unit.support;

import com.atlassian.ta.wiremockpactgenerator.pactgenerator.FileSystem;
import com.atlassian.ta.wiremockpactgenerator.pactgenerator.IdGenerator;
import com.atlassian.ta.wiremockpactgenerator.pactgenerator.PactGenerator;
import com.atlassian.ta.wiremockpactgenerator.pactgenerator.PactGeneratorRequest;
import com.atlassian.ta.wiremockpactgenerator.pactgenerator.PactGeneratorResponse;
import com.atlassian.ta.wiremockpactgenerator.WireMockPactGeneratorUserOptions;
import com.atlassian.ta.wiremockpactgenerator.pactgenerator.PactGeneratorInteraction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PactGeneratorInvocation {
    private final String consumerName;
    private final String providerName;
    private final List<String> requestPathIncludeList;
    private final List<String> requestPathExcludeList;
    private final boolean strictApplicationJson;
    private final List<String> requestHeaderIncludeList;
    private final List<String> responseHeaderIncludeList;
    private final FileSystem fileSystem;
    private final IdGenerator idGenerator;
    private final PactGeneratorRequest request;
    private final PactGeneratorResponse response;
    private final boolean includeNotConfiguredResponses;

    public PactGeneratorInvocation(final FileSystem fileSystem, final IdGenerator idGenerator) {
        this(
                "default-consumer-name",
                "default-provider-name",
                new ArrayList<>(),
                new ArrayList<>(),
                true,
                new ArrayList<>(),
                new ArrayList<>(),
                fileSystem,
                idGenerator,
                new PactGeneratorRequest.Builder()
                        .withMethod("GET")
                        .withUrl("/path")
                        .build(),
                new PactGeneratorResponse.Builder()
                        .withStatus(200)
                        .build(),
                true
        );
    }

    private PactGeneratorInvocation(final String consumerName,
                                    final String providerName,
                                    final List<String> requestPathIncludeList,
                                    final List<String> requestPathExcludeList,
                                    final boolean strictApplicationJson,
                                    final List<String> requestHeaderIncludeList,
                                    final List<String> responseHeaderIncludeList,
                                    final FileSystem fileSystem,
                                    final IdGenerator idGenerator,
                                    final PactGeneratorRequest request,
                                    final PactGeneratorResponse response,
                                    final boolean includeNotConfiguredResponses) {
        this.consumerName = consumerName;
        this.providerName = providerName;
        this.requestPathIncludeList = new ArrayList<>(requestPathIncludeList);
        this.requestPathExcludeList = new ArrayList<>(requestPathExcludeList);
        this.strictApplicationJson = strictApplicationJson;
        this.requestHeaderIncludeList = requestHeaderIncludeList;
        this.responseHeaderIncludeList = responseHeaderIncludeList;
        this.fileSystem = fileSystem;
        this.idGenerator = idGenerator;
        this.request = request;
        this.response = response;
        this.includeNotConfiguredResponses = includeNotConfiguredResponses;
    }

    public PactGeneratorInvocation withConsumer(final String consumerName) {
        return new PactGeneratorInvocation(consumerName, providerName, requestPathIncludeList, requestPathExcludeList,
                strictApplicationJson, requestHeaderIncludeList, responseHeaderIncludeList, fileSystem, idGenerator,
                request, response, includeNotConfiguredResponses);
    }

    public PactGeneratorInvocation withProvider(final String providerName) {
        return new PactGeneratorInvocation(consumerName, providerName, requestPathIncludeList, requestPathExcludeList,
                strictApplicationJson, requestHeaderIncludeList, responseHeaderIncludeList, fileSystem, idGenerator,
                request, response, includeNotConfiguredResponses);
    }

    public PactGeneratorInvocation withRequest(final PactGeneratorRequest request) {
        return new PactGeneratorInvocation(consumerName, providerName, requestPathIncludeList, requestPathExcludeList,
                strictApplicationJson, requestHeaderIncludeList, responseHeaderIncludeList, fileSystem, idGenerator,
                request, response, includeNotConfiguredResponses);
    }

    public PactGeneratorInvocation withResponse(final PactGeneratorResponse response) {
        return new PactGeneratorInvocation(consumerName, providerName, requestPathIncludeList, requestPathExcludeList,
                strictApplicationJson, requestHeaderIncludeList, responseHeaderIncludeList, fileSystem, idGenerator,
                request, response, includeNotConfiguredResponses);
    }

    public PactGeneratorInvocation withPathIncludeList(final String... regexPatterns) {
        final List<String> newRequestPathIncludeList = extendListWithItems(requestPathIncludeList, regexPatterns);
        return new PactGeneratorInvocation(consumerName, providerName, newRequestPathIncludeList, requestPathExcludeList,
                strictApplicationJson, requestHeaderIncludeList, responseHeaderIncludeList, fileSystem, idGenerator,
                request, response, includeNotConfiguredResponses);
    }

    public PactGeneratorInvocation withPathExcludeList(final String... regexPatterns) {
        final List<String> newRequestPathExcludeList = extendListWithItems(requestPathExcludeList, regexPatterns);
        return new PactGeneratorInvocation(consumerName, providerName, requestPathIncludeList, newRequestPathExcludeList,
                strictApplicationJson, requestHeaderIncludeList, responseHeaderIncludeList, fileSystem, idGenerator,
                request, response, includeNotConfiguredResponses);
    }

    public PactGeneratorInvocation withStrictApplicationJson(final boolean strictApplicationJson) {
        return new PactGeneratorInvocation(consumerName, providerName, requestPathIncludeList, requestPathExcludeList,
                strictApplicationJson, requestHeaderIncludeList, responseHeaderIncludeList, fileSystem, idGenerator,
                request, response, includeNotConfiguredResponses);
    }

    public PactGeneratorInvocation withRequestHeaderIncludeList(final String... httpHeaders) {
        final List<String> newRequestHeaderIncludeList = extendListWithItems(responseHeaderIncludeList, httpHeaders);
        return new PactGeneratorInvocation(consumerName, providerName, requestPathIncludeList, requestPathExcludeList,
                strictApplicationJson, newRequestHeaderIncludeList, responseHeaderIncludeList, fileSystem, idGenerator,
                request, response, includeNotConfiguredResponses);
    }

    public PactGeneratorInvocation withResponseHeaderIncludeList(final String... httpHeaders) {
        final List<String> newResponseHeaderIncludeList = extendListWithItems(responseHeaderIncludeList, httpHeaders);
        return new PactGeneratorInvocation(consumerName, providerName, requestPathIncludeList, requestPathExcludeList,
                strictApplicationJson, requestHeaderIncludeList, newResponseHeaderIncludeList, fileSystem, idGenerator,
                request, response, includeNotConfiguredResponses);
    }

    public PactGeneratorInvocation withIncludeNotConfiguredResponses(final boolean includeNotConfiguredResponses) {
        return new PactGeneratorInvocation(consumerName, providerName, requestPathIncludeList, requestPathExcludeList,
                strictApplicationJson, requestHeaderIncludeList, responseHeaderIncludeList, fileSystem, idGenerator,
                request, response, includeNotConfiguredResponses);
    }

    public void invokeProcess() {
        final PactGenerator pactGenerator = createPactGenerator();
        final WireMockPactGeneratorUserOptions userOptions = new WireMockPactGeneratorUserOptions(
                consumerName, providerName, requestPathIncludeList, requestPathExcludeList,
                strictApplicationJson, requestHeaderIncludeList, responseHeaderIncludeList,
                includeNotConfiguredResponses);

        final PactGeneratorInteraction interaction = new PactGeneratorInteraction(
                request,
                response,
                userOptions.getInteractionFilter(),
                userOptions.isStrictApplicationJson(),
                userOptions.getContentFilter()
        );
        pactGenerator.process(interaction);
    }

    public String invokeGetPactLocation() {
        return createPactGenerator().getPactLocation();
    }

    private PactGenerator createPactGenerator() {
        return new PactGenerator(consumerName, providerName, fileSystem, idGenerator);
    }

    private <T> List<T> extendListWithItems(final List<T> original, final T[] items) {
        final List<T> copyOfOriginal = new ArrayList<>(original);
        copyOfOriginal.addAll(Arrays.asList(items));
        return copyOfOriginal;
    }
}
