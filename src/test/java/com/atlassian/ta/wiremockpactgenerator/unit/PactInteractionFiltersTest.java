package com.atlassian.ta.wiremockpactgenerator.unit;

import com.atlassian.ta.wiremockpactgenerator.pactgenerator.FileSystem;
import com.atlassian.ta.wiremockpactgenerator.pactgenerator.IdGenerator;
import com.atlassian.ta.wiremockpactgenerator.WireMockPactGeneratorException;
import com.atlassian.ta.wiremockpactgenerator.pactgenerator.PactGeneratorRequest;
import com.atlassian.ta.wiremockpactgenerator.pactgenerator.PactGeneratorResponse;
import com.atlassian.ta.wiremockpactgenerator.unit.support.PactGeneratorInvocation;
import com.atlassian.ta.wiremockpactgenerator.unit.support.PactFileSpy;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PactInteractionFiltersTest {
    @Mock
    private FileSystem fileSystem;

    @Mock
    private IdGenerator idGenerator;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

    private PactGeneratorInvocation pactGeneratorInvocation;
    private PactFileSpy pactFileSpy;

    @Before
    public void beforeEach() {
        pactGeneratorInvocation = new PactGeneratorInvocation(fileSystem, idGenerator);
        pactFileSpy = new PactFileSpy(fileSystem);
    }

    @Test
    public void shouldNotSaveInteraction_whenRequestPathNotInIncludeList() {
        pactGeneratorInvocation
                .withPathIncludeList("/match/me/.*")
                .withRequest(
                        aDefaultRequest()
                                .withUrl("/wont/match/")
                                .build())
                .invokeProcess();

        pactFileSpy.verifyNoInteractionsSaved();
    }

    @Test
    public void shouldImplicitlyMatchThePathFromTheBeginning() {
        pactGeneratorInvocation
                .withPathIncludeList("/path/")
                .withRequest(
                        aDefaultRequest()
                                .withUrl("/dont/match/this/path/")
                                .build())
                .invokeProcess();

        pactFileSpy.verifyNoInteractionsSaved();
    }

    @Test
    public void shouldImplicitlyMatchThePathToTheEnd() {
        pactGeneratorInvocation
                .withPathIncludeList("/path/")
                .withRequest(
                        aDefaultRequest()
                                .withUrl("/path/wont/get/matched")
                                .build())
                .invokeProcess();

        pactFileSpy.verifyNoInteractionsSaved();
    }

    @Test
    public void shouldSaveInteraction_whenRequestPathMatchesIncludeListItem() {
        pactGeneratorInvocation
                .withPathIncludeList("/match/me/.*")
                .withRequest(
                        aDefaultRequest()
                                .withUrl("/match/me/please")
                                .build())
                .invokeProcess();
        assertThat(pactFileSpy.interactionCount(), is(1));
    }

    @Test
    public void shouldSaveInteraction_whenRequestMatchesAtLeastOneIncludeListItem() {
        pactGeneratorInvocation
                .withPathIncludeList(
                        "/wont/match/path/.*",
                        "/should/match/path/.*")
                .withRequest(
                    aDefaultRequest()
                            .withUrl("/should/match/path/in/this/test")
                            .build())
                .invokeProcess();
        assertThat(pactFileSpy.interactionCount(), is(1));
    }

    @Test
    public void shouldNotConsiderTheQueryStringInTheIncludeListMatching() {
        pactGeneratorInvocation
                .withPathIncludeList(".*/ends-with/path")
                .withRequest(
                        aDefaultRequest()
                            .withUrl("/rest/ends-with/path?with=args")
                            .build())
                .invokeProcess();
        assertThat(pactFileSpy.interactionCount(), is(1));
    }

    @Test
    public void shouldNotIncludeTheUriFragmentInTheIncludeListMatching() {
        pactGeneratorInvocation
                .withPathIncludeList(".*/ends-with/path")
                .withRequest(
                        aDefaultRequest()
                                .withUrl("/rest/ends-with/path#fragment")
                                .build())
                .invokeProcess();
        assertThat(pactFileSpy.interactionCount(), is(1));
    }

    @Test
    public void shouldSaveTheInteractionOnce_whenRequestMatchesMultipleItems() {
        pactGeneratorInvocation
                .withPathIncludeList("/starts-with/.*")
                .withPathIncludeList(".*/ends-with/")
                .withRequest(
                        aDefaultRequest()
                                .withUrl("/starts-with/and/ends-with/")
                                .build())
                .invokeProcess();
        assertThat(pactFileSpy.interactionCount(), is(1));
    }

    @Test
    public void shouldFailIfIncludeListPatternIsInvalid() {
        expectAWireMockPactGeneratorException(
                equalTo("Invalid regex pattern in request path include list"),
                instanceOf(RuntimeException.class),
                pactGeneratorInvocation.withPathIncludeList("*/invalid")
        );
    }

    @Test
    public void shouldNotSaveTheInteraction_whenRequestPathInExcludeList() {
        pactGeneratorInvocation
                .withPathExcludeList("/excluded/.*")
                .withRequest(
                        aDefaultRequest()
                            .withUrl("/excluded/path")
                            .build())
                .invokeProcess();
        pactFileSpy.verifyNoInteractionsSaved();
    }

    @Test
    public void shouldSaveTheInteraction_whenRequestPathNotInExcludeList() {
        pactGeneratorInvocation
                .withPathExcludeList("/excluded/.*")
                .withRequest(
                        aDefaultRequest()
                                .withUrl("/not/excluded/path")
                                .build())
                .invokeProcess();
        assertThat(pactFileSpy.interactionCount(), is(1));
    }

    @Test
    public void shouldNotSaveTheInteraction_whenRequestPathMatchesAtLeastOneItemInExcludeList() {
        pactGeneratorInvocation
                .withPathExcludeList("/excluded/.*", ".*/forbidden/.*")
                .withRequest(
                        aDefaultRequest()
                                .withUrl("/a/forbidden/path")
                                .build())
                .invokeProcess();
        pactFileSpy.verifyNoInteractionsSaved();
    }

    @Test
    public void shouldNotSaveTheInteraction_whenRequestPathMatchesBothIncludeListAndExcludeList() {
        pactGeneratorInvocation
                .withPathExcludeList("/some/path/.*")
                .withPathIncludeList("/some/path/.*")
                .withRequest(
                        aDefaultRequest()
                                .withUrl("/some/path/resource")
                                .build())
                .invokeProcess();
        pactFileSpy.verifyNoInteractionsSaved();
    }

    @Test
    public void shouldSaveTheInteraction_whenRequestPathMatchesIncludeListButNotExcludeList() {
        pactGeneratorInvocation
                .withPathIncludeList("/accept/all/.*")
                .withPathExcludeList("/accept/all/but/this/.*")
                .withRequest(
                        aDefaultRequest()
                                .withUrl("/accept/all/please")
                                .build())
                .invokeProcess();
        assertThat(pactFileSpy.interactionCount(), is(1));
    }

    @Test
    public void shouldFailIfExcludeListPatternIsInvalid() {
        expectAWireMockPactGeneratorException(
                equalTo("Invalid regex pattern in request path exclude list"),
                instanceOf(RuntimeException.class),
                pactGeneratorInvocation.withPathExcludeList("*/invalid")
        );
    }

    @Test
    public void shouldNotSaveTheInteraction_whenResponseWasNotConfiguredAndIncludeNotConfiguredRequestsIsDisabled() {
        pactGeneratorInvocation
                .withIncludeNotConfiguredResponses(false)
                .withResponse(aDefaultResponse()
                        .withIsConfiguredResponse(false)
                        .build())
                .invokeProcess();
        pactFileSpy.verifyNoInteractionsSaved();
    }

    @Test
    public void shouldSaveTheInteraction_whenIncludeNotConfiguredRequestsIsDisabledButResponseWasConfigured() {
        pactGeneratorInvocation
                .withIncludeNotConfiguredResponses(false)
                .withResponse(aDefaultResponse()
                        .withIsConfiguredResponse(true)
                        .build())
                .invokeProcess();
        assertThat(pactFileSpy.interactionCount(), is(1));
    }

    private PactGeneratorRequest.Builder aDefaultRequest() {
        return new PactGeneratorRequest.Builder()
                .withMethod("GET")
                .withUrl("/path");
    }

    private PactGeneratorResponse.Builder aDefaultResponse() {
        return new PactGeneratorResponse.Builder()
                .withStatus(200)
                .withIsConfiguredResponse(true);
    }

    private void expectAWireMockPactGeneratorException(
            final Matcher<String> message,
            final Matcher<Throwable> cause,
            final PactGeneratorInvocation invocation
    ) {
        final Exception exception = Assert.assertThrows(WireMockPactGeneratorException.class, invocation::invokeProcess);
        assertThat(exception.getMessage(), message);
        assertThat(exception.getCause(), cause);
    }
}
