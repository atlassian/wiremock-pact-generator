package com.atlassian.ta.wiremockpactgenerator;

import com.atlassian.ta.wiremockpactgenerator.pactgenerator.PactGeneratorRegistry;
import com.atlassian.ta.wiremockpactgenerator.pactgenerator.PactGeneratorRequest;
import com.atlassian.ta.wiremockpactgenerator.pactgenerator.PactGeneratorResponse;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.http.RequestListener;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class WireMockPactGenerator implements RequestListener {
    private final WireMockPactGeneratorUserOptions userOptions;
    private final Consumer<RuntimeException> unexpectedErrorHandler;

    public static Builder builder(final String consumerName, final String providerName) {
        return new Builder(consumerName, providerName);
    }

    private WireMockPactGenerator(final WireMockPactGeneratorUserOptions userOptions, final Consumer<RuntimeException> unexpectedErrorHandler) {
        this.userOptions = userOptions;
        this.unexpectedErrorHandler = unexpectedErrorHandler;
    }

    @Override
    public void requestReceived(final Request request, final Response response) {
        try {
            processInteraction(request, response);
        } catch (final RuntimeException exception) {
            this.unexpectedErrorHandler.accept(exception);
        }
    }

    public String getPactLocation() {
        return PactGeneratorRegistry.getPactLocation(userOptions);
    }

    private void processInteraction(final Request request, final Response response) {
        final PactGeneratorRequest.Builder requestBuilder = new PactGeneratorRequest.Builder()
                .withMethod(request.getMethod().value())
                .withUrl(request.getUrl())
                .withHeaders(extractHeaders(request.getHeaders()))
                .withBody(request.getBodyAsString());

        final PactGeneratorResponse.Builder responseBuilder = new PactGeneratorResponse.Builder()
                .withStatus(response.getStatus())
                .withHeaders(extractHeaders(response.getHeaders()))
                .withBody(response.getBody() == null ? "" : response.getBodyAsString())
                .withIsConfiguredResponse(response.wasConfigured());

        PactGeneratorRegistry.processInteraction(
                userOptions,
                requestBuilder.build(),
                responseBuilder.build()
        );
    }

    private Map<String, List<String>> extractHeaders(final HttpHeaders wireMockHeaders) {
        final Map<String, List<String>> headers = new HashMap<>();

        for (final HttpHeader header : wireMockHeaders.all()) {
            headers.put(header.key(), header.values());
        }
        return headers;
    }

    public static class Builder {
        private static final Consumer<RuntimeException> defaultUnexpectedErrorHandler = e -> {
            System.err.println("WireMock Pact Generator: unexpected error. Forcing system exit.");
            e.printStackTrace();
            System.exit(1);
        };
        private final List<String> requestPathIncludeList;
        private final List<String> requestPathExcludeList;
        private final String consumerName;
        private final String providerName;
        private final boolean strictApplicationJson;
        private final List<String> requestHeaderIncludeList;
        private final List<String> responseHeaderIncludeList;
        private final Consumer<RuntimeException> unexpectedErrorHandler;
        private final boolean includeNotConfiguredResponses;

        private Builder(final String consumerName, final String providerName) {
            this(
                consumerName,
                providerName,
                Collections.emptyList(),
                Collections.emptyList(),
                true,
                Collections.emptyList(),
                Collections.emptyList(),
                defaultUnexpectedErrorHandler,
                true
            );
        }

        private Builder(final String consumerName,
                        final String providerName,
                        final List<String> requestPathIncludeList,
                        final List<String> requestPathExcludeList,
                        final boolean strictApplicationJson,
                        final List<String> requestHeaderIncludeList,
                        final List<String> responseHeaderIncludeList,
                        final Consumer<RuntimeException> unexpectedErrorHandler,
                        final boolean includeNotConfiguredResponses
        ) {
            this.consumerName = consumerName;
            this.providerName = providerName;
            this.requestPathIncludeList = requestPathIncludeList;
            this.requestPathExcludeList = requestPathExcludeList;
            this.strictApplicationJson = strictApplicationJson;
            this.requestHeaderIncludeList = requestHeaderIncludeList;
            this.responseHeaderIncludeList = responseHeaderIncludeList;
            this.unexpectedErrorHandler = unexpectedErrorHandler;
            this.includeNotConfiguredResponses = includeNotConfiguredResponses;
        }

        /**
         * Specify path patterns of requests that should be recorded as pact interactions (everything if unset).
         * @param regexPatterns List of regex path patterns to match request that should be captured.
         * @return an updated instance of the options builder.
         */
        public Builder withRequestPathIncludeList(final String... regexPatterns) {
            final List<String> newRequestPathIncludeList = extendListWithItems(requestPathIncludeList, regexPatterns);
            return new Builder(
                    consumerName,
                    providerName,
                    newRequestPathIncludeList,
                    requestPathExcludeList,
                    strictApplicationJson,
                    requestHeaderIncludeList,
                    responseHeaderIncludeList,
                    unexpectedErrorHandler,
                    includeNotConfiguredResponses
            );
        }

        /**
         * Specify path patterns of requests that should not be recorded as pact interactions.
         * @param regexPatterns List of regex patterns of request paths that should be omitted.
         * @return an updated instance of the options builder.
         */
        public Builder withRequestPathExcludeList(final String... regexPatterns) {
            final List<String> newRequestPathExcludeList = extendListWithItems(requestPathExcludeList, regexPatterns);
            return new Builder(
                    consumerName,
                    providerName,
                    requestPathIncludeList,
                    newRequestPathExcludeList,
                    strictApplicationJson,
                    requestHeaderIncludeList,
                    responseHeaderIncludeList,
                    unexpectedErrorHandler,
                    includeNotConfiguredResponses
            );
        }

        /**
         * According to [RFC-4627](https://tools.ietf.org/html/rfc4627) Section 2. A valid application/json payload must
         * include as root element either a JSON object or a JSON array.
         *
         * By default, WireMock Pact Generator sticks to this rule. This means that when saving pact files for captured
         * request or response bodies with contents such as `true`, `null`, `33`, `"a quoted string"`, these will not be
         * interpreted as JSON payloads and bodies will be saved as unserialized raw strings (keeping the quotes in the
         * last example).However, for some APIs that are not that strict the default behavior is not desired. In those
         * cases WireMock Pact Generator can be instructed to disable this strict mode and allow serialization of every
         * primitive JSON element.
         * @param strictApplicationJson  enable/disable strict application/json mode (default: true).
         * @return an updated instance of the options builder.
         */
        public Builder withStrictApplicationJson(final boolean strictApplicationJson) {
            return new Builder(
                    consumerName,
                    providerName,
                    requestPathIncludeList,
                    requestPathExcludeList,
                    strictApplicationJson,
                    requestHeaderIncludeList,
                    responseHeaderIncludeList,
                    unexpectedErrorHandler,
                    includeNotConfiguredResponses
            );
        }

        /**
         * Specify what request headers should be recorded in the pact interaction (all headers if unset).
         * @param httpHeaders List of request headers to be captured (case insensitive).
         * @return an updated instance of the options builder.
         */
        public Builder withRequestHeaderIncludeList(final String... httpHeaders) {
            final List<String> newRequestHeaderIncludeList = extendListWithItems(requestHeaderIncludeList, httpHeaders);
            return new Builder(
                    consumerName,
                    providerName,
                    requestPathIncludeList,
                    requestPathExcludeList,
                    strictApplicationJson,
                    newRequestHeaderIncludeList,
                    responseHeaderIncludeList,
                    unexpectedErrorHandler,
                    includeNotConfiguredResponses
            );
        }

        /**
         * Specify what response headers should be recorded in the pact interaction (all headers if unset).
         * @param httpHeaders List of response headers to be captured (case insensitive).
         * @return an updated instance of the options builder.
         */
        public Builder withResponseHeaderIncludeList(final String... httpHeaders) {
            final List<String> newResponseHeaderIncludeList = extendListWithItems(responseHeaderIncludeList, httpHeaders);
            return new Builder(
                    consumerName,
                    providerName,
                    requestPathIncludeList,
                    requestPathExcludeList,
                    strictApplicationJson,
                    requestHeaderIncludeList,
                    newResponseHeaderIncludeList,
                    unexpectedErrorHandler,
                    includeNotConfiguredResponses
            );
        }

        /**
         * There's no mechanism to instruct WireMock to fail when an unexpected error occurs in WireMockPactGenerator
         * for that reason the default behaviour is to terminate the Java process which might be too intrusive. This
         * method allows specifying a custom error handler function for specific projects to implement a more graceful
         * way to fail the build.
         * @param unexpectedErrorHandler function to handle unexpected errors occurring in WireMockPactGenerator.
         * @return an updated instance of the options builder.
         */
        public Builder withUnexpectedErrorHandler(final Consumer<RuntimeException> unexpectedErrorHandler) {
            return new Builder(
                    consumerName,
                    providerName,
                    requestPathIncludeList,
                    requestPathExcludeList,
                    strictApplicationJson,
                    requestHeaderIncludeList,
                    responseHeaderIncludeList,
                    unexpectedErrorHandler,
                    includeNotConfiguredResponses
            );
        }

        /**
         * Build a WireMockPactGenerator request listener for WireMock with the provided settings.
         * @return a WireMockPactGenerator request listener.
         */
        public WireMockPactGenerator build() {
            final WireMockPactGeneratorUserOptions userOptions = new WireMockPactGeneratorUserOptions(
                    consumerName,
                    providerName,
                    requestPathIncludeList,
                    requestPathExcludeList,
                    strictApplicationJson,
                    requestHeaderIncludeList,
                    responseHeaderIncludeList,
                    includeNotConfiguredResponses
            );
            return new WireMockPactGenerator(userOptions, unexpectedErrorHandler);
        }

        /**
         * When a request does not match any stubbing in WireMock a default 404 response is generated. By default,
         * these interactions are captured by WireMockPactGenerator. This behaviour can be disabled by this method.
         * @param includeNotConfiguredResponses false to prevent capturing interactions for responses not configured
         *                                      in WireMock (defaults to true if unset)
         * @return an updated instance of the options builder.
         */
        public Builder withIncludeNotConfiguredResponses(final boolean includeNotConfiguredResponses) {
            return new Builder(
                    consumerName,
                    providerName,
                    requestPathIncludeList,
                    requestPathExcludeList,
                    strictApplicationJson,
                    requestHeaderIncludeList,
                    responseHeaderIncludeList,
                    unexpectedErrorHandler,
                    includeNotConfiguredResponses
            );
        }

        /**
         * Specify path patterns of requests that should be recorded as pact interactions (everything if unset).
         * @deprecated method was renamed, use {@link #withRequestPathIncludeList(String...)} instead.
         * @param regexPatterns List of regex path patterns to match request that should be captured.
         * @return an updated instance of the options builder.
         */
        @Deprecated
        public Builder withRequestPathWhitelist(final String... regexPatterns) {
            return this.withRequestPathIncludeList(regexPatterns);
        }

        /**
         * Specify path patterns of requests that should not be recorded as pact interactions.
         * @deprecated method was renamed, use {@link #withRequestPathExcludeList(String...)} instead.
         * @param regexPatterns List of regex patterns of request paths that should be omitted.
         * @return an updated instance of the options builder.
         */
        @Deprecated
        public Builder withRequestPathBlacklist(final String... regexPatterns) {
            return this.withRequestPathExcludeList(regexPatterns);
        }

        /**
         * Specify what request headers should be recorded in the pact interaction (all headers if unset).
         * @deprecated method was renamed, use {@link #withRequestHeaderIncludeList(String...)} instead.
         * @param httpHeaders List of request headers to be captured (case insensitive).
         * @return an updated instance of the options builder.
         */
        @Deprecated
        public Builder withRequestHeaderWhitelist(final String... httpHeaders) {
            return this.withRequestHeaderIncludeList(httpHeaders);
        }

        /**
         * Specify what response headers should be recorded in the pact interaction (all headers if unset).
         * @deprecated method was renamed, use {@link #withResponseHeaderIncludeList(String...)} instead.
         * @param httpHeaders List of response headers to be captured (case insensitive).
         * @return an updated instance of the options builder.
         */
        @Deprecated
        public Builder withResponseHeaderWhitelist(final String... httpHeaders) {
            return this.withResponseHeaderIncludeList(httpHeaders);
        }

        private <T> List<T> extendListWithItems(final List<T> original, final T[] items) {
            final List<T> copyOfOriginal = new ArrayList<>(original);
            copyOfOriginal.addAll(Arrays.asList(items));
            return copyOfOriginal;
        }
    }
}
