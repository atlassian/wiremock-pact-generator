package com.atlassian.ta.wiremockpactgenerator.pactgenerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ContentFilter {
    private static final String WIREMOCK_MATCHED_STUB_ID_HEADER = "matched-stub-id";
    private static final String WIREMOCK_MATCHED_STUB_NAME_HEADER = "matched-stub-name";

    private static final List<String> IGNORE_REQUEST_HEADERS = Collections.singletonList("host");
    private static final List<String> IGNORE_RESPONSE_HEADERS =
            Arrays.asList(WIREMOCK_MATCHED_STUB_ID_HEADER, WIREMOCK_MATCHED_STUB_NAME_HEADER);
    private final List<String> requestHeaderIncludeList;
    private final List<String> responseHeaderIncludeList;

    public ContentFilter(
            final List<String> requestHeaderIncludeList,
            final List<String> responseHeaderIncludeList
    ) {
        this.requestHeaderIncludeList = requestHeaderIncludeList;
        this.responseHeaderIncludeList = responseHeaderIncludeList;
    }

    public boolean isRequestHeaderInIncludeList(final String headerName) {
        if (requestHeaderIncludeList.isEmpty()) {
            return !IGNORE_REQUEST_HEADERS.contains(headerName);
        }
        return headerMatchesAnyCaseInsensitiveKeys(headerName, requestHeaderIncludeList);
    }

    public boolean isResponseHeaderInIncludeList(final String headerName) {
        if (responseHeaderIncludeList.isEmpty()) {
            return !IGNORE_RESPONSE_HEADERS.contains(headerName);
        }
        return headerMatchesAnyCaseInsensitiveKeys(headerName, responseHeaderIncludeList);
    }

    private static boolean headerMatchesAnyCaseInsensitiveKeys(final String headerName, final List<String> headerIncludeList) {
        return headerIncludeList.stream().anyMatch(key -> key.equalsIgnoreCase(headerName));
    }
}
