package com.atlassian.ta.wiremockpactgenerator.pactgenerator.models;

public class PactCollaborator {
    private final String name;

    public PactCollaborator(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
