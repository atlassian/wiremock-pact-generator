package com.atlassian.ta.wiremockpactgenerator.pactgenerator;

import java.util.List;
import java.util.regex.Pattern;

public class InteractionFilter {
    private final List<Pattern> requestPathIncludeList;
    private final List<Pattern> requestPathExcludeList;
    private final boolean includeNotConfiguredResponses;

    public InteractionFilter(
            final List<Pattern> requestPathIncludeList,
            final List<Pattern> requestPathExcludeList,
            final boolean includeNotConfiguredResponses
    ) {
        this.requestPathIncludeList = requestPathIncludeList;
        this.requestPathExcludeList = requestPathExcludeList;
        this.includeNotConfiguredResponses = includeNotConfiguredResponses;
    }

    public boolean isInteractionAccepted(final PactGeneratorRequest request, final PactGeneratorResponse response) {
        return isAcceptedByPathIncludeList(request) &&
                isAcceptedByPathExcludeList(request) &&
                isResponseConfiguredOrIncludesNotConfiguredResponses(response);
    }

    private boolean isAcceptedByPathIncludeList(final PactGeneratorRequest request) {
        return requestPathIncludeList.isEmpty() || pathMatchesAnyPattern(request.getPath(), requestPathIncludeList);
    }

    private boolean isAcceptedByPathExcludeList(final PactGeneratorRequest request) {
        return !pathMatchesAnyPattern(request.getPath(), requestPathExcludeList);
    }

    private boolean isResponseConfiguredOrIncludesNotConfiguredResponses(final PactGeneratorResponse response) {
        return includeNotConfiguredResponses || response.isConfigured();
    }

    private static boolean pathMatchesAnyPattern(final String path, final List<Pattern> patternList) {
        return patternList.stream().anyMatch(pattern -> pattern.matcher(path).matches());
    }
}
