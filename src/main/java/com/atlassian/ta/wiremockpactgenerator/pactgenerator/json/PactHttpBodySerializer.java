package com.atlassian.ta.wiremockpactgenerator.pactgenerator.json;

import com.atlassian.ta.wiremockpactgenerator.pactgenerator.models.PactHttpBody;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;

public abstract class PactHttpBodySerializer implements JsonSerializer<PactHttpBody> {
    @Override
    public JsonElement serialize(final PactHttpBody body,
                                 final Type type,
                                 final JsonSerializationContext jsonSerializationContext) {
        final String bodyValue = body.getValue();

        if (bodyValue == null) {
            return JsonNull.INSTANCE;
        }

        if (shouldSerializeAsJson(bodyValue)) {
            return JsonParser.parseString(bodyValue);
        }

        return new JsonPrimitive(bodyValue);
    }

    protected abstract boolean shouldSerializeElement(final JsonElement element);

    private boolean shouldSerializeAsJson(final String s) {
        try {
            final JsonElement element = JsonParser.parseString(s);
            return shouldSerializeElement(element);
        } catch (final JsonSyntaxException ex) {
            return false;
        }
    }
}
