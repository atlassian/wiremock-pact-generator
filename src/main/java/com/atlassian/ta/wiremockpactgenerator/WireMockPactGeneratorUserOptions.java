package com.atlassian.ta.wiremockpactgenerator;

import com.atlassian.ta.wiremockpactgenerator.pactgenerator.ContentFilter;
import com.atlassian.ta.wiremockpactgenerator.pactgenerator.InteractionFilter;
import com.atlassian.ta.wiremockpactgenerator.support.Validation;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WireMockPactGeneratorUserOptions {
    private final String consumerName;
    private final String providerName;
    private final List<Pattern> requestPathIncludeList;
    private final List<Pattern> requestPathExcludeList;
    private final boolean strictApplicationJson;
    private final List<String> requestHeaderIncludeList;
    private final List<String> responseHeaderIncludeList;
    private final boolean includeNotConfiguredResponses;

    public WireMockPactGeneratorUserOptions(final String consumerName,
                                            final String providerName,
                                            final List<String> requestPathIncludeList,
                                            final List<String> requestPathExcludeList,
                                            final boolean strictApplicationJson,
                                            final List<String> requestHeaderIncludeList,
                                            final List<String> responseHeaderIncludeList,
                                            final boolean includeNotConfiguredResponses
    ) {
        this.consumerName = Validation.notNullNorBlank(consumerName, "consumer name");
        this.providerName = Validation.notNullNorBlank(providerName, "provider name");
        this.requestPathIncludeList = this.loadPatternListOption(
                requestPathIncludeList, "Invalid regex pattern in request path include list");
        this.requestPathExcludeList = this.loadPatternListOption(
                requestPathExcludeList, "Invalid regex pattern in request path exclude list");
        this.strictApplicationJson = strictApplicationJson;
        this.requestHeaderIncludeList = requestHeaderIncludeList;
        this.responseHeaderIncludeList = responseHeaderIncludeList;
        this.includeNotConfiguredResponses = includeNotConfiguredResponses;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public String getProviderName() {
        return providerName;
    }

    public InteractionFilter getInteractionFilter() {
        return new InteractionFilter(requestPathIncludeList, requestPathExcludeList, includeNotConfiguredResponses);
    }

    public boolean isStrictApplicationJson() {
        return this.strictApplicationJson;
    }

    public ContentFilter getContentFilter() {
        return new ContentFilter(requestHeaderIncludeList, responseHeaderIncludeList);
    }

    private List<Pattern> loadPatternListOption(final List<String> patternList, final String errorMessage) {
        return Validation.withWireMockPactGeneratorExceptionWrapper(
            () -> patternList
                .stream()
                .map(Pattern::compile)
                .collect(Collectors.toList()),
            errorMessage
        );
    }
}
